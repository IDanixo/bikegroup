package com.unrav.bikegroup

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var cancelButton = findViewById<Button>(R.id.cancelLoginButton)
        var loginButton = findViewById<Button>(R.id.loginLoginButton)
        var registerButton = findViewById<Button>(R.id.registerLoginButton)

        var name = findViewById<EditText>(R.id.nameLoginEditText)
        var password = findViewById<EditText>(R.id.passwordLoginEditText)

        var textView = findViewById<TextView>(R.id.textViewLogin)

        loadLogin(name, password)

        cancelButton.setOnClickListener {
            finish()
        }

        loginButton.setOnClickListener {
            login(name.text.toString(), password.text.toString(), textView)
        }

        registerButton.setOnClickListener {
            finish()
            intentRegister()
        }
    }

    fun login(name: String, password: String, textView: TextView) {
        val url = "http://192.168.178.23/login.php"
        val queue = Volley.newRequestQueue(this)


        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                if(response == "login success")
                {
                    storeLogin(name, password)
                    finish()
                    intentProfile()
                }
                else{
                    textView.text = "Wrong password or username"
                    textView.setTextColor(Color.RED)
                }

            },
            { error ->
                textView.text = error.toString()
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {
                val hashMap = HashMap<String, String>()
                hashMap.put("name", name)
                hashMap.put("password", password)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }



    fun storeLogin(name: String, password: String)
    {
        val sharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply(){
            putString("Name", name)
            putString("Password", password)
        }.apply()
    }

    fun loadLogin(nameEdit: EditText, passwordEdit: EditText)
    {
        val sharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE)
        val name = sharedPreferences.getString("Name", null)
        val password = sharedPreferences.getString("Password", null)

       if(name != null){
           nameEdit.setText(name.toString())
       }

        if (password != null){
            passwordEdit.setText(password.toString())
        }
    }

    fun intentProfile(){
        val intent = Intent(this, Profile::class.java)
        startActivity(intent)
    }

    fun intentRegister(){
        val intent = Intent(this, Register::class.java)
        startActivity(intent)
    }
}
