package com.unrav.bikegroup

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class Profile : AppCompatActivity() {

    var name: String? = null
    var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        var cancel = findViewById<Button>(R.id.cancelProfileButton)

        loadLogin()

        if(name != null && password != null)
        {
            var n: String = name.toString()
            var p: String = password.toString()

            loadProfileData(n, p)
        }

        cancel.setOnClickListener {
            finish()
            hub()
        }
    }

    fun hub(){
        val intent = Intent(this, Hub::class.java)
        startActivity(intent)
    }



    fun loadLogin() {
        val sharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE)
        val loadedName = sharedPreferences.getString("Name", null)
        val loadedPassword = sharedPreferences.getString("Password", null)

        name = loadedName
        password = loadedPassword
    }

    fun loadProfileData(name: String, password: String) {

        var nameText = findViewById<TextView>(R.id.nameProfile)
        var motorcycleText = findViewById<TextView>(R.id.motorcycleProfile)
        var expText = findViewById<TextView>(R.id.expProfile)
        var psText = findViewById<TextView>(R.id.psProfile)

        val url = "http://192.168.178.23/getrider.php"
        val queue = Volley.newRequestQueue(this)
        var jsonarr = JSONArray()

        //TO DO NEED TO AVOID DUPLICATED ACCOUNTS

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                jsonarr = JSONArray(response)

                var jsonObj = jsonarr.getJSONObject(0)


                nameText.text = name
                motorcycleText.text = jsonObj.get("motorcycle").toString()
                expText.text = jsonObj.get("experience").toString()
                psText.text = jsonObj.get("ps").toString()


            },
            { error ->
                Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_LONG).show()
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {
                val hashMap = HashMap<String, String>()
                hashMap.put("name", name)
                //hashMap.put("password", password)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }
}
