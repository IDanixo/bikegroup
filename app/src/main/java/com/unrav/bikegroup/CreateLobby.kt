package com.unrav.bikegroup

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main


class CreateLobby : AppCompatActivity() {

    private var nextEmptyLobbyID: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_lobby)

        initSpinnerCities()
        initButtonsAndAddEvents()
        getHighestIdFromLobbie()
    }

    fun getHighestIdFromLobbie() {
        val url = "http://192.168.178.23/gethighestlobbyid.php"
        val queue = Volley.newRequestQueue(this)


        val stringRequest = object : StringRequest(
            Request.Method.GET, url,
            { response ->

                var i = response.toInt()
                i += 1
                nextEmptyLobbyID = i
            },
            {
            }) {
        }
        queue.add(stringRequest)
    }

    fun booleanToIntMySQL(boolean: Boolean): String {
        return if (boolean) "1" else "0"
    }

    fun clearSwitchColor(switch: Switch) {
        switch.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
    }

    fun createLobby(createLobbyButton: Button) {
        val errorSwitch = ContextCompat.getColor(this, R.color.errorSwitch)

        var superSportSwitch = findViewById<Switch>(R.id.superSportSwitch)
        var nakedSwitch = findViewById<Switch>(R.id.nakedSwitch)
        var adventureBikeSwitch = findViewById<Switch>(R.id.adventureBikeSwitch)
        var harleyDavidsonSwitch = findViewById<Switch>(R.id.harleyDavidsonSwitch)
        var motocrossSwitch = findViewById<Switch>(R.id.motocrossSwitch)
        var scooterSwitch = findViewById<Switch>(R.id.scooterSwitch)

        var fiftyCcmSwitch = findViewById<Switch>(R.id.fiftyCcmSwitch)
        var oneHundredTwentyFiveCcmSwitch = findViewById<Switch>(R.id.oneHundredTwentyFiveCcmSwitch)
        var twoHundredFiftyCcmSwitch = findViewById<Switch>(R.id.twoHundredFiftyCcmSwitch)
        var threeHundredFiftyCcmSwitch = findViewById<Switch>(R.id.threeHundredFiftyCcmSwitch)
        var sixHundredCcmSwitch = findViewById<Switch>(R.id.sixHundredCcmSwitch)
        var oneThousandCcmSwitch = findViewById<Switch>(R.id.oneThousandCcmSwitch)

        // Get Booleans or Values of switches
        var superSportSwitchResult = booleanToIntMySQL(superSportSwitch.isChecked)
        var nakedSwitchResult = booleanToIntMySQL(nakedSwitch.isChecked)
        var adventureBikeSwitchResult = booleanToIntMySQL(adventureBikeSwitch.isChecked)
        var harleyDavidsonSwitchResult = booleanToIntMySQL(harleyDavidsonSwitch.isChecked)
        var motocrossSwitchResult = booleanToIntMySQL(motocrossSwitch.isChecked)
        var scooterSwitchResult = booleanToIntMySQL(scooterSwitch.isChecked)

        var shortRideSwitchResult =
            booleanToIntMySQL(findViewById<Switch>(R.id.shortRideSwitch).isChecked)
        var longRideSwitchResult =
            booleanToIntMySQL(findViewById<Switch>(R.id.longRideSwitch).isChecked)
        var adventureRideSwitchResult =
            booleanToIntMySQL(findViewById<Switch>(R.id.adventureRideSwitch).isChecked)

        var groupSizeResult = findViewById<EditText>(R.id.groupSizeEditText).text.toString()

        //Advertisement Duration
        var advertisementDurationResult: String = "0"
        if (findViewById<Switch>(R.id.oneHourSwitch).isChecked) {
            advertisementDurationResult = "1"
        } else if (findViewById<Switch>(R.id.threeHourSwitch).isChecked) {
            advertisementDurationResult = "3"
        } else if (findViewById<Switch>(R.id.twelvehoursSwitch).isChecked) {
            advertisementDurationResult = "12"
        }

        var citySpinnerResult = findViewById<Spinner>(R.id.citySpinner).selectedItem.toString()

        var fiftyCcmSwitchResult = booleanToIntMySQL(fiftyCcmSwitch.isChecked)
        var oneHundredTwentyFiveCcmSwitchResult =
            booleanToIntMySQL(oneHundredTwentyFiveCcmSwitch.isChecked)
        var twoHundredFiftyCcmSwitchResult = booleanToIntMySQL(twoHundredFiftyCcmSwitch.isChecked)
        var threeHundredFiftyCcmSwitchResult =
            booleanToIntMySQL(threeHundredFiftyCcmSwitch.isChecked)
        var sixHundredCcmSwitchResult = booleanToIntMySQL(sixHundredCcmSwitch.isChecked)
        var oneThousandCcmSwitchResult = booleanToIntMySQL(oneThousandCcmSwitch.isChecked)

        //At least one Motorcycle switch must be on
        if (!superSportSwitch.isChecked && !nakedSwitch.isChecked && !adventureBikeSwitch.isChecked && !harleyDavidsonSwitch.isChecked && !motocrossSwitch.isChecked && !scooterSwitch.isChecked) {
            superSportSwitch.setBackgroundColor(errorSwitch)
            nakedSwitch.setBackgroundColor(errorSwitch)
            adventureBikeSwitch.setBackgroundColor(errorSwitch)
            harleyDavidsonSwitch.setBackgroundColor(errorSwitch)
            motocrossSwitch.setBackgroundColor(errorSwitch)
            scooterSwitch.setBackgroundColor(errorSwitch)

            Toast.makeText(
                applicationContext,
                "WARNING: Please select motorcycle!",
                Toast.LENGTH_LONG
            ).show()

            CoroutineScope(Main).launch {
                delay(3000L)
                clearSwitchColor(superSportSwitch)
                clearSwitchColor(nakedSwitch)
                clearSwitchColor(adventureBikeSwitch)
                clearSwitchColor(harleyDavidsonSwitch)
                clearSwitchColor(motocrossSwitch)
                clearSwitchColor(scooterSwitch)
            }

        } else if (!fiftyCcmSwitch.isChecked && !oneHundredTwentyFiveCcmSwitch.isChecked && !twoHundredFiftyCcmSwitch.isChecked && !threeHundredFiftyCcmSwitch.isChecked && !sixHundredCcmSwitch.isChecked && !oneThousandCcmSwitch.isChecked) {
            fiftyCcmSwitch.setBackgroundColor(errorSwitch)
            oneHundredTwentyFiveCcmSwitch.setBackgroundColor(errorSwitch)
            twoHundredFiftyCcmSwitch.setBackgroundColor(errorSwitch)
            threeHundredFiftyCcmSwitch.setBackgroundColor(errorSwitch)
            sixHundredCcmSwitch.setBackgroundColor(errorSwitch)
            oneThousandCcmSwitch.setBackgroundColor(errorSwitch)

            Toast.makeText(
                applicationContext,
                "WARNING: Please select the allowed ccm!",
                Toast.LENGTH_LONG
            ).show()

            CoroutineScope(Main).launch {
                delay(3000L)
                clearSwitchColor(fiftyCcmSwitch)
                clearSwitchColor(oneHundredTwentyFiveCcmSwitch)
                clearSwitchColor(twoHundredFiftyCcmSwitch)
                clearSwitchColor(threeHundredFiftyCcmSwitch)
                clearSwitchColor(sixHundredCcmSwitch)
                clearSwitchColor(oneThousandCcmSwitch)
            }
        } else {

            //START CREATION REQUEST---------------------------------------------------------
            val url = "http://192.168.178.23/joinlobby.php"
            val queue = Volley.newRequestQueue(this)


            val stringRequest = object : StringRequest(
                Request.Method.POST, url,
                { response ->

                    //createLobbyButton.text = "Response is: ${response}"
                    Toast.makeText(applicationContext, "Lobby created.$response", Toast.LENGTH_LONG)
                        .show()
                    finish()

                },
                {
                    //createLobbyButton.text = "That didn't work!"
                    Toast.makeText(
                        applicationContext,
                        "WARNING: No Lobby created!",
                        Toast.LENGTH_LONG
                    ).show()
                }) {
                //Press Ctr + O to find getParams
                override fun getParams(): MutableMap<String, String> {
                    val hashMap = HashMap<String, String>()
                    hashMap.put("idlobby", "7")
                    hashMap.put("leader", "TEST")
                    hashMap.put("riderid", "7")
                    hashMap.put("supersport", superSportSwitchResult)
                    hashMap.put("naked", nakedSwitchResult)
                    hashMap.put("adventure", adventureBikeSwitchResult)
                    hashMap.put("harleydavidson", harleyDavidsonSwitchResult)
                    hashMap.put("motocross", motocrossSwitchResult)
                    hashMap.put("scooter", scooterSwitchResult)
                    hashMap.put("shortride", shortRideSwitchResult)
                    hashMap.put("longride", longRideSwitchResult)
                    hashMap.put("adventureride", adventureRideSwitchResult)
                    hashMap.put("groupsize", groupSizeResult)
                    hashMap.put("advertisementduration", advertisementDurationResult)
                    hashMap.put("city", citySpinnerResult)
                    hashMap.put("50ccm", fiftyCcmSwitchResult)
                    hashMap.put("125ccm", oneHundredTwentyFiveCcmSwitchResult)
                    hashMap.put("250ccm", twoHundredFiftyCcmSwitchResult)
                    hashMap.put("350ccm", threeHundredFiftyCcmSwitchResult)
                    hashMap.put("600ccm", sixHundredCcmSwitchResult)
                    hashMap.put("1000ccm", oneThousandCcmSwitchResult)
                    return hashMap
                }
            }
            queue.add(stringRequest)
        }
    }

    fun createGroup() {

        var superSportSwitch = findViewById<Switch>(R.id.superSportSwitch)
        var nakedSwitch = findViewById<Switch>(R.id.nakedSwitch)
        var adventureBikeSwitch = findViewById<Switch>(R.id.adventureBikeSwitch)
        var harleyDavidsonSwitch = findViewById<Switch>(R.id.harleyDavidsonSwitch)
        var motocrossSwitch = findViewById<Switch>(R.id.motocrossSwitch)
        var scooterSwitch = findViewById<Switch>(R.id.scooterSwitch)

        var shortRideSwitch = findViewById<Switch>(R.id.shortRideSwitch)
        var longRideSwitch = findViewById<Switch>(R.id.longRideSwitch)
        var adventureRideSwitch = findViewById<Switch>(R.id.adventureRideSwitch)

        var groupSize = findViewById<EditText>(R.id.groupSizeEditText)

        var oneHourSwitch = findViewById<Switch>(R.id.oneHourSwitch)
        var threeHourSwitch = findViewById<Switch>(R.id.threeHourSwitch)
        var twelveHourSwitch = findViewById<Switch>(R.id.twelvehoursSwitch)

        var citySpinner = findViewById<Spinner>(R.id.citySpinner)

        var fiftyCcmSwitch = findViewById<Switch>(R.id.fiftyCcmSwitch)
        var oneHundredTwentyFiveCcmSwitch = findViewById<Switch>(R.id.oneHundredTwentyFiveCcmSwitch)
        var twoHundredFiftyCcmSwitch = findViewById<Switch>(R.id.twoHundredFiftyCcmSwitch)
        var threeHundredFiftyCcmSwitch = findViewById<Switch>(R.id.threeHundredFiftyCcmSwitch)
        var sixHundredCcmSwitch = findViewById<Switch>(R.id.sixHundredCcmSwitch)
        var oneThousandCcmSwitch = findViewById<Switch>(R.id.oneThousandCcmSwitch)

    }

    fun initButtonsAndAddEvents() {

        var superSportSwitch = findViewById<Switch>(R.id.superSportSwitch)
        var nakedSwitch = findViewById<Switch>(R.id.nakedSwitch)
        var adventureBikeSwitch = findViewById<Switch>(R.id.adventureBikeSwitch)
        var harleyDavidsonSwitch = findViewById<Switch>(R.id.harleyDavidsonSwitch)
        var motocrossSwitch = findViewById<Switch>(R.id.motocrossSwitch)
        var scooterSwitch = findViewById<Switch>(R.id.scooterSwitch)

        var shortRideSwitch = findViewById<Switch>(R.id.shortRideSwitch)
        var longRideSwitch = findViewById<Switch>(R.id.longRideSwitch)
        var adventureRideSwitch = findViewById<Switch>(R.id.adventureRideSwitch)

        var groupSize = findViewById<EditText>(R.id.groupSizeEditText)

        var oneHourSwitch = findViewById<Switch>(R.id.oneHourSwitch)
        var threeHourSwitch = findViewById<Switch>(R.id.threeHourSwitch)
        var twelveHourSwitch = findViewById<Switch>(R.id.twelvehoursSwitch)

        var citySpinner = findViewById<Spinner>(R.id.citySpinner)

        var fiftyCcmSwitch = findViewById<Switch>(R.id.fiftyCcmSwitch)
        var oneHundredTwentyFiveCcmSwitch = findViewById<Switch>(R.id.oneHundredTwentyFiveCcmSwitch)
        var twoHundredFiftyCcmSwitch = findViewById<Switch>(R.id.twoHundredFiftyCcmSwitch)
        var threeHundredFiftyCcmSwitch = findViewById<Switch>(R.id.threeHundredFiftyCcmSwitch)
        var sixHundredCcmSwitch = findViewById<Switch>(R.id.sixHundredCcmSwitch)
        var oneThousandCcmSwitch = findViewById<Switch>(R.id.oneThousandCcmSwitch)

        var createLobbyButton = findViewById<Button>(R.id.createGroupButton)
        var cancelButton = findViewById<Button>(R.id.cancelButton)

        createLobbyButton.setOnClickListener {
            createLobby(createLobbyButton)
        }
        cancelButton.setOnClickListener {
            finish()
        }

        shortRideSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (shortRideSwitch.isChecked) {
                shortRideSwitch.isClickable = false
                longRideSwitch.isChecked = false
                longRideSwitch.isClickable = true
                adventureRideSwitch.isChecked = false
                adventureRideSwitch.isClickable = true
            }
        }
        longRideSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (longRideSwitch.isChecked) {
                longRideSwitch.isClickable = false
                shortRideSwitch.isChecked = false
                shortRideSwitch.isClickable = true
                adventureRideSwitch.isChecked = false
                adventureRideSwitch.isClickable = true
            }
        }
        adventureRideSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (adventureRideSwitch.isChecked) {
                adventureRideSwitch.isClickable = false
                longRideSwitch.isChecked = false
                longRideSwitch.isClickable = true
                shortRideSwitch.isChecked = false
                shortRideSwitch.isClickable = true
            }
        }

        oneHourSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (oneHourSwitch.isChecked) {
                oneHourSwitch.isClickable = false
                threeHourSwitch.isChecked = false
                threeHourSwitch.isClickable = true
                twelveHourSwitch.isChecked = false
                twelveHourSwitch.isClickable = true
            }
        }
        threeHourSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (threeHourSwitch.isChecked) {
                threeHourSwitch.isClickable = false
                oneHourSwitch.isChecked = false
                oneHourSwitch.isClickable = true
                twelveHourSwitch.isChecked = false
                twelveHourSwitch.isClickable = true
            }
        }
        twelveHourSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (twelveHourSwitch.isChecked) {
                twelveHourSwitch.isClickable = false
                oneHourSwitch.isChecked = false
                oneHourSwitch.isClickable = true
                threeHourSwitch.isChecked = false
                threeHourSwitch.isClickable = true
            }
        }
    }

    fun initSpinnerCities() {
        val spinner: Spinner = findViewById(R.id.citySpinner)
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.city_names,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
    }
}