package com.unrav.bikegroup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Start : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        supportActionBar?.hide()

        var start = findViewById<Button>(R.id.startButton)

        start.setOnClickListener {
            finish()
            startLogin()
        }
    }

    fun startLogin(){
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
    }
}