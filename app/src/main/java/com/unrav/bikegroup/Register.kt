package com.unrav.bikegroup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main

class Register : AppCompatActivity() {

    var preRegisterNameBoolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        var registerButton = findViewById<Button>(R.id.registerButton)
        var cancelRegisterButton = findViewById<Button>(R.id.registerCancelButton)

        registerButton.setOnClickListener {
            registerCycle(registerButton)
        }

        cancelRegisterButton.setOnClickListener {
            finish()
        }
    }

    fun registerCycle(button: Button) {
        var name = findViewById<EditText>(R.id.name).text.toString()
        preRegisterCheckNoNameExists(name)
    }

    fun setAndClearEditTextColor(editText: EditText) {
        val errorSwitch = ContextCompat.getColor(this, R.color.errorSwitch)

        editText.setBackgroundColor(errorSwitch)

        CoroutineScope(Main).launch {
                delay(3000L)
            editText.setBackgroundColor(ContextCompat.getColor(editText.context, R.color.transparent))

        }
    }

    fun preregister(preRegisterCheckNameBoolean: Boolean):Boolean {


        var nameButton = findViewById<EditText>(R.id.name)
        var pass1Button = findViewById<EditText>(R.id.pass1)
        var pass2Button = findViewById<EditText>(R.id.pass2)
        var motorcycleButton = findViewById<EditText>(R.id.motorcycle)
        var experienceButton = findViewById<EditText>(R.id.experience)
        var psButton = findViewById<EditText>(R.id.ps)

        var name = nameButton.text.toString()
        var pass = pass1Button.text.toString()
        var motorcycle = motorcycleButton.text.toString()
        var experience = experienceButton.text.toString()
        var ps = psButton.text.toString()

        if(name == "")
        {
            setAndClearEditTextColor(nameButton)
            return false
        }

        if(pass == "")
        {
            setAndClearEditTextColor(pass1Button)
            setAndClearEditTextColor(pass2Button)
            return false
        }

        if(motorcycle == "")
        {
            setAndClearEditTextColor(motorcycleButton)
            return false
        }

        if(experience == "")
        {
            setAndClearEditTextColor(experienceButton)
            return false
        }

        if(ps == "")
        {
            setAndClearEditTextColor(psButton)
            return false
        }

        if(!preRegisterCheckNameBoolean)
        {
            setAndClearEditTextColor(nameButton)
            return false
        }

        if (pass1Button.text.toString() == pass2Button.text.toString()) {
                register(name, pass, motorcycle, experience, ps)
            return true
        } else {
                setAndClearEditTextColor(pass1Button)
                setAndClearEditTextColor(pass2Button)
            return false
        }
    }

    fun preRegisterCheckNoNameExists(name: String) {
        val errorSwitch = ContextCompat.getColor(this, R.color.errorSwitch)

        val url = "http://192.168.178.23/checkname.php"
        val queue = Volley.newRequestQueue(this)

        var registerButton = findViewById<Button>(R.id.registerButton)

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                registerButton.text = response

                if (response == "no name") {
                    preregister(true)
                }
                else
                {
                    preregister(false)
                }
            },
            {
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("name", name)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    fun register(name: String, password: String, motorcycle: String, experience: String, ps: String) {
        val url = "http://192.168.178.23/register.php"
        val queue = Volley.newRequestQueue(this)


        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->
                Toast.makeText(applicationContext, "Registration success!", Toast.LENGTH_LONG)
                finish()

            },
            {
                Toast.makeText(applicationContext, "Registration failed!", Toast.LENGTH_LONG)
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {


                val hashMap = HashMap<String, String>()
                hashMap.put("name", name)
                hashMap.put("password", password)
                hashMap.put("motorcycle", motorcycle)
                hashMap.put("experience", experience)
                hashMap.put("ps", ps)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }
}
