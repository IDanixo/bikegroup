package com.unrav.bikegroup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlin.collections.List

class Hub : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hub)

        var createLobby: Button = findViewById(R.id.createLobbyButtonHub)
        var joinLobby: Button = findViewById(R.id.joinLobbyButtonHub)

        createLobby.setOnClickListener {
            createLobby()
        }

        joinLobby.setOnClickListener {
            list()
        }
    }

    fun createLobby(){
        val intent = Intent(this, CreateLobby::class.java)
        startActivity(intent)
    }

    fun list(){
        val intent = Intent(this, com.unrav.bikegroup.List::class.java)
        startActivity(intent)
    }


}