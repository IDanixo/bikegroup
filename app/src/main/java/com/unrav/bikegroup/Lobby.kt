package com.unrav.bikegroup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.partybox.view.*
import kotlinx.android.synthetic.main.partylist.view.*
import org.json.JSONArray
import org.json.JSONObject

class Lobby : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lobby)

        var test = findViewById<LinearLayout>(R.id.partybox_activity)
        var lobby = findViewById<LinearLayout>(R.id.lobbyLayout)
        var backButton = findViewById<Button>(R.id.lobbyBackButton)

        backButton.setOnClickListener {
            finish()
            val intent = Intent(this, List::class.java)
            startActivity(intent)
        }


        //var dd = layoutInflater.inflate(R.layout.partybox, lobby)
        //layoutInflater.inflate(R.layout.partybox, lobby)
        //layoutInflater.inflate(R.layout.partybox, lobby)
        //layoutInflater.inflate(R.layout.partybox, lobby)
        //layoutInflater.inflate(R.layout.partybox, lobby)

        var lobbyID: String = intent.getStringExtra("lobbyID").toString()




        getLobbyJson(lobbyID, backButton)

    }

    fun getLobbyJson(idlobby: String, testButton: Button) {
        val errorSwitch = ContextCompat.getColor(this, R.color.errorSwitch)

        //var jsonobj = JSONObject()
        //val jsonarr = JSONArray()
        val url = "http://192.168.178.23/getlobby.php"
        val queue = Volley.newRequestQueue(this)

        var lobby = findViewById<LinearLayout>(R.id.lobbyLayout)
        var registerButton = findViewById<Button>(R.id.registerButton)

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                Toast.makeText(applicationContext, response, Toast.LENGTH_LONG).show()

                val jsonarr = JSONArray(response)

                var i = 0

                var dd: View

                // First inflate Views after this change values
                while (i < jsonarr.length())
                {
                    layoutInflater.inflate(R.layout.partybox, lobby)
                    i++
                }

                //Needs to start at 1 because 0 is Activity Button
                i = 1

                while (i <= jsonarr.length()) {

                    //Fix because starts at 1
                    var jsonobj = jsonarr.getJSONObject(i - 1)

                    var dd = lobby.getChildAt(i)

                    //dd = layoutInflater.inflate(R.layout.partybox, lobby)

                    testButton.text = "$i"

                    jsonobj.get("idlobby").toString()
                    dd.userNameBox.text = jsonobj.get("leader").toString()

                    //TODO in the future make an extra request for rider bike name
                    //var bikeName = jsonobj.get("").toString()
                    changeToCustomBikeImage("test", dd, i, jsonarr.length())

                    jsonobj.get("riderid").toString()
                    if (jsonobj.get("supersport").toString() == "1") {
                        dd.typeBox.text = "Supersport"
                    }
                    if (jsonobj.get("naked").toString() == "1") {
                        dd.typeBox.text = "Naked"
                    }
                    if (jsonobj.get("adventure").toString() == "1") {
                        dd.typeBox.text = "Adventure"
                    }
                    if (jsonobj.get("harleydavidson").toString() == "1") {
                        dd.typeBox.text = "Harley"
                    }
                    if (jsonobj.get("motocross").toString() == "1") {
                        dd.typeBox.text = "Motocross"
                    }
                    if (jsonobj.get("scooter").toString() == "1") {
                        dd.typeBox.text = "Scooter"
                    }
                    jsonobj.get("shortride").toString()
                    jsonobj.get("longride").toString()
                    jsonobj.get("adventureride").toString()
                    jsonobj.get("groupsize").toString()
                    jsonobj.get("advertisementduration").toString()
                    jsonobj.get("city").toString()
                    jsonobj.get("50ccm").toString()
                    jsonobj.get("125ccm").toString()
                    jsonobj.get("250ccm").toString()
                    jsonobj.get("350ccm").toString()
                    jsonobj.get("600ccm").toString()
                    jsonobj.get("1000ccm").toString()
                    i++
                }
            },
            { error ->
                testButton.text = error.toString()
                Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_LONG).show()

            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("idlobby", idlobby)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    fun changeToCustomBikeImage(bikeName: String, childref:  View, riderNumber: Int, riderTotal: Int){

        var modulo = 4
        var myRiderNumber = riderNumber
        if (myRiderNumber < modulo)
        {
            myRiderNumber += modulo
        }
        var x = myRiderNumber % modulo
        when (x) {
            0 -> {
                Toast.makeText(applicationContext, "X: 0", Toast.LENGTH_LONG).show()
                childref.imageView.setImageResource(R.drawable.bike)
            }
            1 -> {
                Toast.makeText(applicationContext, "X: 1", Toast.LENGTH_LONG).show()
                childref.imageView.setImageResource(R.drawable.z1000750)
            }
            2 -> {
                Toast.makeText(applicationContext, "X: 2", Toast.LENGTH_LONG).show()
                childref.imageView.setImageResource(R.drawable.zx10r750)
            }
            3 -> {
                Toast.makeText(applicationContext, "X: 3", Toast.LENGTH_LONG).show()
                childref.imageView.setImageResource(R.drawable.h2750)
            }
        }
    }

    fun getLobby(idlobby: String, testButton: Button) {
        val errorSwitch = ContextCompat.getColor(this, R.color.errorSwitch)

        val url = "http://192.168.178.23/getlobby.php"
        val queue = Volley.newRequestQueue(this)

        var registerButton = findViewById<Button>(R.id.registerButton)

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                testButton.text = response

            },
            {
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("idlobby", idlobby)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }
}