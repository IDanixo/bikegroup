package com.unrav.bikegroup

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.children
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.partylist.view.*
import org.json.JSONArray
import org.json.JSONObject

class List : AppCompatActivity(), CallBackListener {

    // Filter text size
    private var textSize = 10.0f
    // PartyList is starting at child number
    private var partyListStartingChildAtPos = 6
    // Make sure it is not the first launch
    // Hack to avoid firing two onItemSelected Events at startUp
    //private var isStartUp = true
    // Hack to count this to 2, because onitemselected is called on startup twice and need
    // to avoid first two filter being created
    private var onItemSelectedCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        var selectedItems = mutableListOf<String>()

        var listBackButton = findViewById<Button>(R.id.listBackButton)

        spinners(selectedItems)

        listBackButton.setOnClickListener {
            finish()
        }
        getFilterResult()
    }

    fun customButtonOnClick(selectedItems: MutableList<String>, button: Button)
    {
        var parentLinear = button.parent

        if(parentLinear is LinearLayout)
        {
            parentLinear.removeView(button)
            selectedItems.remove(button.text.toString())
        }
        getFilterResult()
    }

    fun spinners(selectedItems: MutableList<String>) {

        var motorTypeSpinner = findViewById<Spinner>(R.id.motorTypeSpinner)
        var motorSizeSpinner = findViewById<Spinner>(R.id.motorSizeSpinner)

        var resultLayout = findViewById<LinearLayout>(R.id.resultLinearLayout)
        var resultLayout2 = findViewById<LinearLayout>(R.id.resultLinearLayout2)
        var resultLayout3 = findViewById<LinearLayout>(R.id.resultLinearLayout3)

        // -------------------- Create Adapters for Dropdown to work ---------------------------
        var adapter = ArrayAdapter.createFromResource(
            this,
            R.array.type_motorcycles,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        motorTypeSpinner.adapter = adapter

        adapter = ArrayAdapter.createFromResource(
            this,
            R.array.motor_sizes,
            android.R.layout.simple_spinner_item
        )

        motorSizeSpinner.adapter = adapter

        motorTypeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(onItemSelectedCounter < 2)
                {
                    onItemSelectedCounter++
                    return
                }

                var button = Button(applicationContext)
                button.textSize = textSize
                button.setTypeface(Typeface.DEFAULT, Typeface.BOLD)
                button.setOnClickListener { customButtonOnClick(selectedItems, button) }

                if (p0 != null) {

                    button.text = p0.getItemAtPosition(p2).toString()

                    if (!selectedItems.contains(p0.getItemAtPosition(p2).toString())) {
                        if (resultLayout.childCount < 4) {
                            resultLayout.addView(button)
                            selectedItems.add(p0.getItemAtPosition(p2).toString())
                        } else if (resultLayout2.childCount < 4) {
                            resultLayout2.addView(button)
                            selectedItems.add(p0.getItemAtPosition(p2).toString())
                        } else if (resultLayout3.childCount < 4) {
                            resultLayout3.addView(button)
                            selectedItems.add(p0.getItemAtPosition(p2).toString())
                        }
                    }
                }
                getFilterResult()
            }
        }

        motorSizeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(onItemSelectedCounter < 2)
                {
                    onItemSelectedCounter++
                    return
                }

                var button = Button(applicationContext)
                button.textSize = textSize
                button.setTypeface(Typeface.DEFAULT, Typeface.BOLD)
                button.setOnClickListener { customButtonOnClick(selectedItems, button) }

                if (p0 != null) {

                    button.text = p0.getItemAtPosition(p2).toString()

                    if (!selectedItems.contains(p0.getItemAtPosition(p2).toString())) {
                        if (resultLayout.childCount < 4) {
                            resultLayout.addView(button)
                            selectedItems.add(p0.getItemAtPosition(p2).toString())
                        } else if (resultLayout2.childCount < 4) {
                            resultLayout2.addView(button)
                            selectedItems.add(p0.getItemAtPosition(p2).toString())
                        } else if (resultLayout3.childCount < 4) {
                            resultLayout3.addView(button)
                            selectedItems.add(p0.getItemAtPosition(p2).toString())
                        }
                    }
                }
                getFilterResult()
            }
        }
    }

    // getFilterResult calls automatic getCustomMYQLQuery
    // getCustomMYSQLQuery calls getListRequest automatically
    fun getFilterResult(){

        var linearlayout1 = findViewById<LinearLayout>(R.id.resultLinearLayout)
        var linearlayout2 = findViewById<LinearLayout>(R.id.resultLinearLayout2)
        var linearlayout3 = findViewById<LinearLayout>(R.id.resultLinearLayout3)

        var fifty = false
        var onehundredfiftyfive = false
        var twohundredfifty = false
        var threehundredfifty = false
        var sixhundred = false
        var onethousand = false

        var supersport = false
        var naked = false
        var adventure = false
        var harley = false
        var motocross = false
        var scooter = false

        //Check active Filter and apply in Booleans
        //TODO MAKE THIS LOOP CLEANER
        for (s in linearlayout1.children){
            if(s is Button){
                if(s.text == resources.getStringArray(R.array.motor_sizes)[0]){
                    fifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[1]){
                    onehundredfiftyfive = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[2]){
                    twohundredfifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[3]){
                    threehundredfifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[4]){
                    sixhundred = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[5]){
                    onethousand = true
                }

                if(s.text == resources.getStringArray(R.array.type_motorcycles)[0]){
                    supersport = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[1]){
                    naked = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[2]){
                    adventure = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[3]){
                    harley = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[4]){
                    motocross = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[5]){
                    scooter = true
                }
            }
        }

        for (s in linearlayout2.children){
            if(s is Button){
                if(s.text == resources.getStringArray(R.array.motor_sizes)[0]){
                    fifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[1]){
                    onehundredfiftyfive = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[2]){
                    twohundredfifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[3]){
                    threehundredfifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[4]){
                    sixhundred = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[5]){
                    onethousand = true
                }

                if(s.text == resources.getStringArray(R.array.type_motorcycles)[0]){
                    supersport = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[1]){
                    naked = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[2]){
                    adventure = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[3]){
                    harley = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[4]){
                    motocross = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[5]){
                    scooter = true
                }
            }
        }

        for (s in linearlayout3.children){
            if(s is Button){
                if(s.text == resources.getStringArray(R.array.motor_sizes)[0]){
                    fifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[1]){
                    onehundredfiftyfive = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[2]){
                    twohundredfifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[3]){
                    threehundredfifty = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[4]){
                    sixhundred = true
                }
                if(s.text == resources.getStringArray(R.array.motor_sizes)[5]){
                    onethousand = true
                }

                if(s.text == resources.getStringArray(R.array.type_motorcycles)[0]){
                    supersport = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[1]){
                    naked = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[2]){
                    adventure = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[3]){
                    harley = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[4]){
                    motocross = true
                }
                if(s.text == resources.getStringArray(R.array.type_motorcycles)[5]){
                    scooter = true
                }
            }
        }

        getCustomMYSQLQuery(fifty, onehundredfiftyfive, twohundredfifty, threehundredfifty, sixhundred, onethousand, supersport, naked, adventure, harley, motocross, scooter)
    }

    fun emptyList(list: LinearLayout){
        list.removeViews(6, list.childCount - 6)
    }

    // getCustomMYSQLQuery calls getListRequest automatically
    fun getCustomMYSQLQuery(xFifty: Boolean, xOneHundredFifty: Boolean, xTwoHundredFifty: Boolean, xThreeHundredFifty: Boolean, xSixHundred: Boolean, xOneThousand: Boolean,
                            xSuperSport: Boolean, xNaked: Boolean, xAdventure: Boolean, xHarley: Boolean, xMotocross: Boolean, xScooter: Boolean){

        var mysql_qry = "SELECT * FROM lobbies WHERE "
        var mysql_qry_start_length = mysql_qry.length

        var and = "OR "
        var fifty = "50ccm LIKE '1' " + and
        var onehundredtwentyfive = "125ccm LIKE '1' " + and
        var twohundredfifty = "250ccm LIKE '1' " + and
        var threehundredfifty = "350ccm LIKE '1' " + and
        var sixhundred = "600ccm LIKE '1' " + and
        var onethousand = "1000ccm LIKE '1' " + and

        var supersport = "supersport LIKE '1' " + and
        var naked = "naked LIKE '1' " + and
        var adventure = "adventure LIKE '1' " + and
        var harley = "harleydavidson LIKE '1' " + and
        var motocross = "motocross LIKE '1' " + and
        var scooter = "scooter LIKE '1' " + and

        if(xFifty){
            mysql_qry += fifty
        }
        if(xOneHundredFifty){
            mysql_qry += onehundredtwentyfive
        }
        if(xTwoHundredFifty){
            mysql_qry += twohundredfifty
        }
        if(xThreeHundredFifty){
            mysql_qry += threehundredfifty
        }
        if(xSixHundred){
            mysql_qry += sixhundred
        }
        if(xOneThousand){
            mysql_qry += onethousand
        }

        if(xSuperSport){
            mysql_qry += supersport
        }
        if(xNaked){
            mysql_qry += naked
        }
        if(xAdventure){
            mysql_qry += adventure
        }
        if(xHarley){
            mysql_qry += harley
        }
        if(xMotocross){
            mysql_qry += motocross
        }
        if(xScooter){
            mysql_qry += scooter
        }

        if(mysql_qry.length == mysql_qry_start_length){
            mysql_qry = "SELECT * FROM lobbies;"
        }
        else{
            //Cut off last word "AND "
            mysql_qry = mysql_qry.substring(0, mysql_qry.length - 4)
            mysql_qry += ";"
        }

        var backButton = findViewById<Button>(R.id.listBackButton)
        //backButton.text = mysql_qry
        //Toast.makeText(applicationContext, mysql_qry, Toast.LENGTH_LONG).show()
        getListRequest(mysql_qry)
    }

    fun getListRequest(mysql_qry: String) {

        var backButton = findViewById<Button>(R.id.listBackButton)

        val url = "http://192.168.178.23/getlist.php"
        val queue = Volley.newRequestQueue(this)

        var list = findViewById<LinearLayout>(R.id.listLayout)

        //EmptyList
        /* list.removeViews(6, list.childCount - 6)*/
        emptyList(list)

        //TODO TESTING VALUES
        val motorcycle = "supersport"
        val ccm = "1000ccm"

        //TODO NEED TO AVOID DUPLICATED ACCOUNTS

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                var jsonarr = JSONArray(response)

                val objectList = mutableListOf<JSONObject>()

                var x = 0
                for (i in 0..jsonarr.length() - 1) {
                    objectList.add(jsonarr.getJSONObject(i))
                    x++
                }

                objectList.sortBy { obj -> obj.get("idlobby").toString() }

                var existingIDNumbers = mutableListOf(0)
                var lastPartySize = 1
                //Number for the first child to start - [0] is Button [1] is TextView [2] is LinearLayout (hidden) [3] is LinearLayout [4] is LinearLayout [5] is LinearLayout [6] is first listItem
                //So child starts with 6, but starts with 5, because we increment before using it
                var childNumber = partyListStartingChildAtPos - 1

                // Inflate PartyList in List
                var y = 6
                for (i in 0 until objectList.size){

                    if(i > 0){
                        if(objectList[i].get("idlobby").toString().toInt() == objectList[i - 1].get("idlobby").toString().toInt()){

                        }
                        else{
                            layoutInflater.inflate(R.layout.partylist, list)
                            customListImage(list, y)
                            y++
                        }
                    }
                    else{
                        layoutInflater.inflate(R.layout.partylist, list)
                        customListImage(list, y)
                        y++
                    }
                }

                //Set OnClick Listener for every VIEWLIST BUTTON PartyList created
                for(i in partyListStartingChildAtPos until list.childCount) {
                    list.getChildAt(i).viewListButton.setOnClickListener {
                        //INTENT TO LOBBY PAGE
                        finish()
                        val intent = Intent(this, Lobby::class.java)
                        intent.putExtra(
                            "lobbyID",
                            list.getChildAt(i).partyListLobbyId.text.toString()
                        )
                        startActivity(intent)
                    }
                }

                //Set OnClick Listener for every JOIN BUTTON PartyList created
                for(i in partyListStartingChildAtPos until list.childCount){
                    list.getChildAt(i).joinListButton.setOnClickListener {
                        var profileData = ProfileLoader()
                        profileData.refresh(this)
                        var name = profileData.getName()
                        var password = profileData.getPassword()

                        //Toast.makeText(applicationContext, "Name: $name Password: $password", Toast.LENGTH_LONG).show()
                        backButton.text = "Name: $name Password: $password"

                        var database = Database(this)
                        //database.checkRiderExistsinLobby("1")
                        if (name != null){
                            database.setRiderIDInProfileLoader(name)
                        }
                        profileData.refresh(this)
                        var ownID = profileData.getOwnID()

                        if(ownID != null){
                            database.checkRiderExistsinLobby(list.getChildAt(i).partyLeader.text.toString(), ownID, list.getChildAt(i).partyListLobbyId.text.toString())
                        }
                        Toast.makeText(applicationContext, list.getChildAt(i).partyListLobbyId.text, Toast.LENGTH_LONG).show()

                        //TODO
                        //test this out https://stackoverflow.com/questions/28120029/how-can-i-return-value-from-function-onresponse-of-volley
                        //Toast.makeText(applicationContext, profileData.getOwnID().toString(), Toast.LENGTH_LONG).show()
                        //callBack

                        //Check if rider exists already
                        var riderExistsAlready = false
                        if(name != null){
                            //Toast.makeText(applicationContext, name, Toast.LENGTH_LONG).show()
                        }

                        //Add rider to lobby or skip
                        if(!riderExistsAlready){
                        }

                        //when not insert into lobby
                        list.getChildAt(i).partyListLobbyId

                        list.getChildAt(i).joinListButton.text = "TEST IS SUCCESSFULL! "

                        //INTENT TO LOBBY PAGE
                        finish()
                        val intent = Intent(this, Lobby::class.java)
                        intent.putExtra("lobbyID", list.getChildAt(i).partyListLobbyId.text.toString())
                        startActivity(intent)
                    }
                }

                // Set values for every inflated PartyList
                // Counter adds up for every database item
                var counter = 0
                // New Row starts at 6
                var newRow = partyListStartingChildAtPos
                // Custom lobby ID
                var lobbyID = 1
                for (i in 0 until objectList.size){

                    if(i > 0){
                        if(objectList[i].get("idlobby").toString().toInt() == objectList[i - 1].get("idlobby").toString().toInt()){
                            counter++
                            list.getChildAt(newRow).partySizeList.text = "Members: " + counter.toString()
                            list.getChildAt(newRow).partyListLobbyId.text = lobbyID.toString()
                            list.getChildAt(newRow).partyLeader.text = objectList[i].get("leader").toString()
                        }
                        else{
                            counter = 0
                            newRow++
                            lobbyID++
                            counter++
                            list.getChildAt(newRow).partySizeList.text = counter.toString()
                            //
                            list.getChildAt(newRow).partyLeader.text = objectList[i].get("leader").toString()
                            list.getChildAt(newRow).partyListLobbyId.text = lobbyID.toString()
                            list.getChildAt(newRow).partySizeList.text = "Members: " + counter.toString()
                        }
                    }
                    else{
                        counter++
                        list.getChildAt(newRow).partySizeList.text = counter.toString()
                        //
                        list.getChildAt(newRow).partyLeader.text = objectList[i].get("leader").toString()
                        list.getChildAt(newRow).partyListLobbyId.text = lobbyID.toString()
                        list.getChildAt(newRow).partySizeList.text = "Members: " + counter.toString()
                    }
                }

            },
            { error ->
                Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_LONG).show()
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {
                val hashMap = HashMap<String, String>()
                hashMap.put("mysql", mysql_qry)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    fun customListImage(list: LinearLayout, y: Int){
        //TODO add more pictures to make this system make sense
        var x = y % 6
        when (x) {
            0 -> {
                list.getChildAt(y).partyListLayout.setBackgroundResource(R.drawable.rainyroad)
            }
            1 -> {
                list.getChildAt(y).partyListLayout.setBackgroundResource(R.drawable.desert)
            }
            2 -> {
                list.getChildAt(y).partyListLayout.setBackgroundResource(R.drawable.galaxyroad)
            }
            3 -> {
                list.getChildAt(y).partyListLayout.setBackgroundResource(R.drawable.roadred)
            }
            4 -> {
                list.getChildAt(y).partyListLayout.setBackgroundResource(R.drawable.rainyroad)
            }
            5 -> {
                list.getChildAt(y).partyListLayout.setBackgroundResource(R.drawable.desert)
            }
        }
    }

    override fun callBack(result: String) {
        TODO("Not yet implemented")
    }
}