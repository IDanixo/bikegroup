package com.unrav.bikegroup

interface CallBackListener {

    fun callBack(result: String)
}