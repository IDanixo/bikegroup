package com.unrav.bikegroup

import android.content.Context
import android.provider.ContactsContract

class ProfileLoader {

    private var name: String? = null
    private var password: String? = null
    private var id: String? = null

    fun refresh(context: Context) {
        val sharedPreferences = context.getSharedPreferences("Login", Context.MODE_PRIVATE)
        val loadedName = sharedPreferences.getString("Name", null)
        val loadedPassword = sharedPreferences.getString("Password", null)
        val loadedID = sharedPreferences.getString("ID", null)

        name = loadedName
        password = loadedPassword
        id = loadedID
    }

    fun getOwnID() : String?{
        return id
    }

    //Database only
    //Database class needs to call this method and set the ID
    fun setOwnRiderID(id: String, context: Context) {
        val sharedPreferences = context.getSharedPreferences("Login", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply(){
            putString("ID", id)
        }.apply()
    }

    fun getName() : String?{
        return name
    }

    fun getPassword() : String?{
        return password
    }
}