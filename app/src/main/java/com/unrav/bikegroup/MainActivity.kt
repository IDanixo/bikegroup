package com.unrav.bikegroup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.util.Log
import androidx.annotation.NonNull
import com.google.android.gms.ads.*
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var loginButton: Button = findViewById(R.id.loginButton)
        var startNewActivity: Button = findViewById(R.id.startNewActivity)
        var interstitialAd: Button = findViewById(R.id.interstitialAd)
        var rewardedAdButton: Button = findViewById(R.id.rewardedAdButton)
        var registerButton: Button = findViewById(R.id.registerMain)
        var createLobbyButton: Button = findViewById(R.id.createLobbyButton)
        var lobbyButton: Button = findViewById(R.id.lobbyButton)
        var startButton: Button = findViewById(R.id.startStartButton)
        var profileButton: Button = findViewById(R.id.profileButton)
        var listButton: Button = findViewById(R.id.listButton)

        adMobBanner()

        //Interstitial AD Init
        var interstitialAdx = InterstitialAd(this)
        interstitialAdx.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        interstitialAdx.loadAd(AdRequest.Builder().build())

        //Rewarded Video AD Init
        var rewardedAd = RewardedAd(this, "ca-app-pub-3940256099942544/5224354917")
        initadMobRewardedAd(rewardedAd)

        lobbyButton.setOnClickListener {
            lobby()
        }

        createLobbyButton.setOnClickListener {
            createLobby()
        }

        loginButton.setOnClickListener(){
            login()
        }

        startNewActivity.setOnClickListener(){
            startNewActivity()
        }

        interstitialAd.setOnClickListener(){
            adMobInterstitial(interstitialAdx)
        }

        rewardedAdButton.setOnClickListener(){
            adMobRewardedAd(rewardedAd)
        }

        registerButton.setOnClickListener {
            register()
        }

        startButton.setOnClickListener {
            startscreen()
        }

        profileButton.setOnClickListener {
            profile()
        }

        listButton.setOnClickListener {
            list()
        }

        startscreen()
    }

    fun list(){
        val intent = Intent(this, List::class.java)
        startActivity(intent)
    }

    fun profile(){
        val intent = Intent(this, Profile::class.java)
        startActivity(intent)
    }

    fun login(){
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
    }

    fun startscreen(){
        val intent = Intent(this, Start::class.java)
        startActivity(intent)
    }

    fun lobby(){
        val intent = Intent(this, Lobby::class.java)
        startActivity(intent)
    }

    fun createLobby(){
        val intent = Intent(this, CreateLobby::class.java)
        startActivity(intent)
    }

    fun register(){
        val intent = Intent(this, Register::class.java)
        startActivity(intent)
    }

    fun adMobBanner(){
        MobileAds.initialize(this@MainActivity)

        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    fun adMobInterstitial(interstitialAd: InterstitialAd){

        if(interstitialAd.isLoaded){
            interstitialAd.show()
        }
        else {
            Log.d("TAG", "The interstitial wasnt loaded yet.")
        }
    }

    fun initadMobRewardedAd(rewardedAd: RewardedAd) {
        val adLoadCallback = object : RewardedAdLoadCallback() {
            override fun onRewardedAdLoaded() {
                // Ad successfully loaded.
            }

            override fun onRewardedAdFailedToLoad(adError: LoadAdError) {
                // Ad failed to load.
            }
        }
        rewardedAd.loadAd(AdRequest.Builder().build(), adLoadCallback)
    }

    fun adMobRewardedAd(rewardedAd: RewardedAd) {

        if (rewardedAd.isLoaded) {
            val adCallback = object : RewardedAdCallback() {
                override fun onRewardedAdOpened() {
                    // Ad opened.
                }

                override fun onRewardedAdClosed() {
                    // Ad closed.
                }

                override fun onUserEarnedReward(@NonNull reward: RewardItem) {
                    // User earned reward.
                }

                override fun onRewardedAdFailedToShow(adError: AdError) {
                    // Ad failed to display.
                }

            }
            rewardedAd.show(this@MainActivity, adCallback)
        }
        else {
            Log.d("TAG", "The rewareded ad wasnt loaded yet.")
        }
    }

    fun startNewActivity(){
        val intent = Intent(this, CreateLobby::class.java)
        startActivity(intent)
    }

    fun testxx(button: Button){
        //Testing MYSQL response to variable
        val url = "http://192.168.178.23/testxx.php"
        val queue = Volley.newRequestQueue(this)

        val stringRequest = StringRequest(Request.Method.GET, url,
            { response ->
                
                                        // NICE
                button.text = "${response.toInt() + 1}"
            },
            { button.text = "That didn't work!" })

        queue.add(stringRequest)
    }

    fun joinLobby(button: Button){
        val url = "http://192.168.178.23/joinlobby.php"
        val queue = Volley.newRequestQueue(this)


        val stringRequest = object :StringRequest(Request.Method.POST, url,
            { response ->

                button.text = "Response is: ${response}"

            },
            { button.text = "That didn't work!" })
        {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {
                val hashMap = HashMap<String, String>()
                hashMap.put("name", "yy")
                hashMap.put("password", "222221")
                hashMap.put("motorcycle", "Aprilia RS 660")
                hashMap.put("experience", "9")
                hashMap.put("ps", "200")
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    fun closeLobby(button: Button)
    {
        val url = "http://192.168.178.23/closelobby.php"
        val queue = Volley.newRequestQueue(this)

        val stringRequest = StringRequest(Request.Method.GET, url,
            { response ->
                button.text = "Response is: ${response}"
            },
            { button.text = "That didn't work!" })
        queue.add(stringRequest)
    }

    fun testPost(button: Button)
    {
        val url = "http://192.168.178.23/insertcode.php"
        val queue = Volley.newRequestQueue(this)


        val stringRequest = object :StringRequest(Request.Method.POST, url,
            { response ->
                button.text = "Response is: ${response}"
            },
            { button.text = "That didn't work!" })
        {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {
                val hashMap = HashMap<String, String>()
                hashMap.put("name", "hans")
                hashMap.put("password", "222221")
                hashMap.put("motorcycle", "Aprilia RS 660")
                hashMap.put("experience", "9")
                hashMap.put("ps", "115")
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    fun testLogin(button: Button)
    {
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url = "http://192.168.178.23/login.php"
        val google = "https://www.google.com"

// Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, url,
            { response ->
                // Display the first 500 characters of the response string.
                button.text = "Response is: ${response}"
            },
            { button.text = "That didn't work!" })

// Add the request to the RequestQueue.
        queue.add(stringRequest)
    }

    fun inflate(btn: Button)
    {
        //btn.text = "test"
        btn.isActivated = false

        val linearLayout: LinearLayout = findViewById(R.id.linearLayout)
        val layoutInflater:LayoutInflater = LayoutInflater.from(applicationContext)

        for (i in 0..5){
            val myView: View = layoutInflater.inflate(R.layout.partybox, null, false)
            linearLayout.addView(myView)
        }
    }
}