package com.unrav.bikegroup

import android.content.Context
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject


class Database {
    private val getRiderURL = "http://192.168.178.23/getrider.php"
    private lateinit var myContext: Context
    private val stringInitError = "String not initialized Error"



    constructor(context: Context) {
        myContext = context
    }


    fun setRiderIDInProfileLoader(name: String) {
        val url = "http://192.168.178.23/getrider.php"
        var queue = Volley.newRequestQueue(myContext)

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                var jsonArray = JSONArray(response)

                var id = jsonArray.getJSONObject(0).get("id").toString()

                Toast.makeText(myContext, "RiderID TO SAVE IS $id", Toast.LENGTH_LONG).show()

                var profileLoader = ProfileLoader()
                if(id != null){
                    profileLoader.setOwnRiderID(id, myContext)
                }

            },
            { error ->

                //result = error.toString()

            }) {
            override fun getParams(): MutableMap<String, String> {
                val hashMap = HashMap<String, String>()
                hashMap.put("name", name)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    //-> than get Leader info
    fun checkRiderExistsinLobby(leader: String, riderID: String, lobbyIdToJoin: String){

        val url = "http://192.168.178.23/getriderinlobby.php"
        val queue = Volley.newRequestQueue(myContext)

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                if(response == "true"){
                    Toast.makeText(myContext, "Rider does already exist in lobby", Toast.LENGTH_LONG).show()

                }
                else{
                    getLobbyLeaderINFO(leader, riderID, lobbyIdToJoin)
                }
            },
            {
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("riderid", riderID)
                return hashMap
            }
        }
        queue.add(stringRequest)

    }

    fun getLobbyLeaderINFO(leader: String, riderID: String, lobbyIdToJoin: String) {

        val url = "http://192.168.178.23/getleader.php"
        val queue = Volley.newRequestQueue(myContext)

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                var jsonArray = JSONArray(response)

                var leaderinfo = jsonArray.getJSONObject(0)

                joinLobby(leaderinfo, riderID, lobbyIdToJoin)

            },
            {
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("name", leader)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    private fun getRiderToJoinLobby(riderid: String, lobbyid: String){
        val url = "http://192.168.178.23/getriderbyid.php"
        val queue = Volley.newRequestQueue(myContext)


        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->
                Toast.makeText(myContext, "RiderID is: $riderid -> $response", Toast.LENGTH_LONG).show()

            },
            {
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("id", riderid)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }

    //TODO
    private fun joinLobby(leaderinfo: JSONObject, riderid: String, idlobby: String) {

        val url = "http://192.168.178.23/joinlobby2.php"
        val queue = Volley.newRequestQueue(myContext)

        var leader = leaderinfo.get("leader").toString()
        var supersport = leaderinfo.get("supersport").toString()
        var naked = leaderinfo.get("naked").toString()
        var adventure = leaderinfo.get("adventure").toString()
        var harleydavidson = leaderinfo.get("harleydavidson").toString()
        var motocross = leaderinfo.get("motocross").toString()
        var scooter = leaderinfo.get("scooter").toString()
        var shortride = leaderinfo.get("shortride").toString()
        var longride = leaderinfo.get("longride").toString()
        var adventureride = leaderinfo.get("adventureride").toString()
        var groupsize = leaderinfo.get("groupsize").toString()
        var advertisementduration = leaderinfo.get("advertisementduration").toString()
        var city = leaderinfo.get("city").toString()
        var ffccm = leaderinfo.get("50ccm").toString()
        var offccm = leaderinfo.get("125ccm").toString()
        var tff = leaderinfo.get("250ccm").toString()
        var thff = leaderinfo.get("350ccm").toString()
        var sff = leaderinfo.get("600ccm").toString()
        var onek = leaderinfo.get("1000ccm").toString()

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            { response ->

                if(response == "insert success"){
                    Toast.makeText(myContext, "Rider successfully joined group!", Toast.LENGTH_LONG).show()
                }
                else{
                    Toast.makeText(myContext, "OH NO! Rider not inserted! -> $response", Toast.LENGTH_LONG).show()
                }

            },
            {
            }) {
            //Press Ctr + O to find getParams
            override fun getParams(): MutableMap<String, String> {

                val hashMap = HashMap<String, String>()
                hashMap.put("idlobby", idlobby)
                hashMap.put("leader", leader)
                hashMap.put("riderid", riderid)
                hashMap.put("supersport", supersport)
                hashMap.put("naked", naked)
                hashMap.put("adventure", adventure)
                hashMap.put("harleydavidson", harleydavidson)
                hashMap.put("motocross", motocross)
                hashMap.put("scooter", scooter)
                hashMap.put("shortride", shortride)
                hashMap.put("longride", longride)
                hashMap.put("adventureride", adventureride)
                hashMap.put("groupsize", groupsize)
                hashMap.put("advertisementduration", advertisementduration)
                hashMap.put("city", city)
                hashMap.put("50ccm", ffccm)
                hashMap.put("125ccm", offccm)
                hashMap.put("250ccm", tff)
                hashMap.put("350ccm", thff)
                hashMap.put("600ccm", sff)
                hashMap.put("1000ccm", onek)
                return hashMap
            }
        }
        queue.add(stringRequest)
    }
}

